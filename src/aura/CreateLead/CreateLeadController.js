({
    doInit: function(component, event, helper) {
        // initialize page path attribute
        component.set("v.url", window.location.pathname);
        console.log("Current page path: " + window.location.pathname);
         
        // Prepare a new record from template
        component.find("leadRecordCreator").getNewRecord(
            "Lead", // sObject type (objectApiName)
            null,      // recordTypeId
            false,     // skip cache?
            $A.getCallback(function() {
                var rec = component.get("v.newLead");
                var error = component.get("v.newLeadError");
                if(error || (rec === null)) {
                    console.log("Error initializing record template: " + error);
                    return;
                }
                console.log("Record template initialized: " + rec.apiName);
            })
        );
    },

    handleSaveLead: function(component, event, helper) {
        	component.set("v.url", window.location.pathname); // set path into url attribute
            component.set("v.simpleNewLead.LeadSource", "Web"); // set LeadSource - should be done in the flow instead of hard-coding
        	component.set("v.simpleNewLead.Source_URL__c", window.location.pathname); // set Source URL to current page path
        	console.log("v.simpleNewLead.Source_URL__c: " + component.get("v.simpleNewLead.Source_URL__c")); // used for debugging
        
            // save the record 
        	component.find("leadRecordCreator").saveRecord(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    // record is saved successfully - show popup message
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Success!",
                        "message": "Your details have been submitted successfully."
                    });
                    resultsToast.fire();
                    component.set("v.isFormVisible", "false"); // hide the form
                    component.set("v.isFormSubmitted", "true");// show Thankyou Message

                } else if (saveResult.state === "INCOMPLETE") {
                    // handle the incomplete state
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    // handle the error state
                    var errorString = JSON.stringify(saveResult.error);
                    console.log('Problem saving lead, error: ' + errorString);
                    // Log error to console, not to UI
                    /*var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Problem saving",
                        "message": "error: " + errorString
                    });
                    resultsToast.fire();*/ 
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + errorString);
                }
            });
        
    }
    

})