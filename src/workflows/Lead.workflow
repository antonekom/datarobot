<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Marketing_Manager_New_Lead_Alert</fullName>
        <ccEmails>tonykom@gmail.com</ccEmails>
        <description>Marketing Manager New Lead Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>tk_mmanager@force.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DataRobot_Email_Templates/New_Lead_Alert</template>
    </alerts>
</Workflow>
